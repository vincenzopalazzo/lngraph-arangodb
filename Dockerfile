FROM adoptopenjdk/openjdk14:x86_64-alpine-jdk-14.0.2_12-slim
LABEL mantainer="Vincenzo Palazzo vincenzopalazzodev@gmail.com"

WORKDIR workdir

COPY . .

CMD ["/bin/sh", "entrypoint.sh"]