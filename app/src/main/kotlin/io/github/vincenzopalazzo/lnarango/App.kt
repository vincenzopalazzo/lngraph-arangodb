package io.github.vincenzopalazzo.lnarango

import io.github.vincenzopalazzo.lnarango.AppKeys.ACTION_BUILD_GRAPH
import io.github.vincenzopalazzo.lnarango.actions.ActionMediator
import io.github.vincenzopalazzo.lnarango.actions.strategies.RetrievalStrategy
import io.github.vincenzopalazzo.lnarango.actions.strategies.Strategy
import io.github.vincenzopalazzo.lnarango.model.AppModel
import io.github.vincenzopalazzo.lnarango.persistence.arangodb.DAODatabase
import io.github.vincenzopalazzo.lnarango.utils.HttpRequestFactory
import jrpc.clightning.service.CLightningConfigurator
import mu.KotlinLogging

/**
 * @author https://github.com/vincenzopalazzo
 */
object App {

    private val LOGGER = KotlinLogging.logger {  }

    fun run() {
        if (!System.getenv().containsKey("ARANGO_PASS"))
            throw IllegalArgumentException("The arangodb(ARANGO_PASS) pass need to be in the system env")
        val baseUrl = System.getenv()["LIGHTNING_REST_BASE_URL"].takeUnless { it.isNullOrBlank() } ?: ""
        val nodeURL = System.getenv()[AppKeys.LN_NODE_URL].takeUnless { it.isNullOrBlank() } ?: ""
        val arangoHost = System.getenv("ARANGO_HOST").takeUnless { it.isNullOrBlank() } ?: "localhost"
        val arangoPort = System.getenv("ARANGO_PORT").takeUnless { it.isNullOrBlank() } ?: "8529"
        val arangoPass = System.getenv()["ARANGO_PASS"]!!
        val lightningPath = System.getenv()["SOCKET_PATH"]
        LOGGER.debug { "Arango host %s on port %s".format(arangoHost, arangoPort) }

        val options = HashMap<String, Any>()

        if (baseUrl.isNotEmpty())
            HttpRequestFactory.changeBaseUrl(baseUrl)
        else
            options[AppKeys.LN_NODE_URL] = nodeURL


        DAODatabase.initDatabase(host = arangoHost, pass = arangoPass)

        val retrieval: RetrievalStrategy = when(lightningPath) {
            null -> RetrievalStrategy()
            else -> {
                CLightningConfigurator.getInstance().changeUrlRpcFile(lightningPath)
                RetrievalStrategy(Strategy.UNIX)
            }
        }
        AppModel.putObject(RetrievalStrategy::class.java.canonicalName, retrieval)

        try {
            val actions = ArrayList<String>()
            actions.add(ACTION_BUILD_GRAPH)
            actions.forEach { action ->
                ActionMediator.run(action, options)
            }
        }catch (e: Exception) {
            LOGGER.error { e }
        }
    }
}

fun main() {
    App.run()
}
