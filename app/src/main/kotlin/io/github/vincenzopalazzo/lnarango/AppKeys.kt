package io.github.vincenzopalazzo.lnarango

/**
 * @author https://github.com/vincenzopalazzo
 */
object AppKeys {
    // Object model keys
    const val DAO_GRAPH = "DAO_GRAPH"
    // action keys
    const val ACTION_BUILD_GRAPH = "ACTION_BUILD_GRAPH"
    // lightning node connection
    const val LN_NODE_URL = "LN_NODE_URL"
}
