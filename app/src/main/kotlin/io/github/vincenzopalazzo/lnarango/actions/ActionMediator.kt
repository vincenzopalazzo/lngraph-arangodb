package io.github.vincenzopalazzo.lnarango.actions

import io.github.vincenzopalazzo.lnarango.AppKeys
import io.github.vincenzopalazzo.lnarango.actions.cmd.BuildGraph
import io.github.vincenzopalazzo.lnarango.actions.cmd.ICmd

/**
 * @author https://github.com/vincenzopalazzo
 */
object ActionMediator: IMediator {

    private val actions = mutableMapOf<String, ICmd<*>>()

    init {
        actions[AppKeys.ACTION_BUILD_GRAPH] = BuildGraph()
    }

    override fun run(actionKey: String, opts: HashMap<String, Any>) {
        if (!actionKey.contains(actionKey))
            throw IllegalArgumentException("Unsupported action %s".format(actionKey))
        actions[actionKey]!!.run(opts)
    }
}