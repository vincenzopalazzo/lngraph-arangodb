package io.github.vincenzopalazzo.lnarango.actions

/**
 * @author https://github.com/vincenzopalazzo
 */
interface IMediator {
    /**
     * Run the action with the {key}
     */
    fun run(actionKey: String, opts: HashMap<String, Any> = mapOf<String, Any>() as HashMap<String, Any>)
}