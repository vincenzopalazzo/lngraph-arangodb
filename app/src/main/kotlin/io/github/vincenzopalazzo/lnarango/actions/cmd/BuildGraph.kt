package io.github.vincenzopalazzo.lnarango.actions.cmd

import io.github.vincenzopalazzo.lnarango.AppKeys
import io.github.vincenzopalazzo.lnarango.actions.strategies.RetrievalStrategy
import io.github.vincenzopalazzo.lnarango.model.AppModel
import io.github.vincenzopalazzo.lnarango.model.cmd.Result
import io.github.vincenzopalazzo.lnarango.persistence.arangodb.DAODatabase
import io.github.vincenzopalazzo.lnarango.persistence.arangodb.DAOGraph
import io.github.vincenzopalazzo.lnarango.utils.GraphNodeBuilder
import io.github.vincenzopalazzo.lnarango.utils.JSONConverter
import mu.KotlinLogging

/**
 * @author https://github.com/vincenzopalazzo
 */
class BuildGraph : ICmd<Void?> {

    companion object {
        private val LOGGER = KotlinLogging.logger { }
    }

    override fun run(options: HashMap<String, Any>): Result<Void?> {
        LOGGER.info { "Build graph" }

        val retrievalStrategy = AppModel.getObject(RetrievalStrategy::class.java.canonicalName) as RetrievalStrategy
        val listChannels = retrievalStrategy.listChannels()

        LOGGER.info { "Receive data" }

        val graphDb = DAOGraph()
        graphDb.createGraph(DAODatabase, "LightningNetwork")
        LOGGER.info { "Size graph ${listChannels.channels.size}" }
        for (channel in listChannels.channels) {
            // If the shortChannelId it is null, means that the bitcoin blockchain
            // have not confirmed the open channel transaction.
            // not an important details in terms of the demo!
            if (channel.channelId == null) {
                LOGGER.info { "Short channel id is null, so I will skip this" }
                continue
            }
            LOGGER.info { "${channel.source} --> ${channel.destination}" }
            val sourceInfo = retrievalStrategy.getNodeInfo(channel.source)
            LOGGER.info { "${channel.source}: ${JSONConverter.serialize(sourceInfo)}" }
            val destinationInfo = retrievalStrategy.getNodeInfo(channel.destination)
            LOGGER.info { "${channel.destination}: ${JSONConverter.serialize(destinationInfo)}" }
            val from = GraphNodeBuilder.build(
                label = sourceInfo.nodeId,
                value = mapOf(
                    "addresses" to sourceInfo.addresses,
                    "alias" to sourceInfo.alias,
                    "color" to sourceInfo.color,
                    "last_update" to sourceInfo.lastTimestamp
                )
            )
            val to = GraphNodeBuilder.build(
                label = destinationInfo.nodeId,
                value = mapOf(
                    "addresses" to destinationInfo.addresses,
                    "alias" to destinationInfo.alias,
                    "color" to destinationInfo.color,
                    "last_update" to destinationInfo.lastTimestamp
                )
            )
            val options = HashMap<String, Any>()
            options["channel_id"] = channel.channelId ?: "unknown"
            options["capacity_sat"] = channel.satoshis
            options["base_fee_msat"] = channel.baseFeeMilliSatoshi
            options["fee_per_msat"] = channel.feePerMillionth
            LOGGER.trace { "${from.label} | ${JSONConverter.serialize(options)} | ${to.label}" }
            graphDb.insertEdge(from, to, options)
        }

        AppModel.putObject(AppKeys.DAO_GRAPH, graphDb)
        return Result(null)
    }
}