package io.github.vincenzopalazzo.lnarango.actions.cmd

import io.github.vincenzopalazzo.lnarango.model.cmd.Result

/**
 * @author https://github.com/vincenzopalazzo
 */
interface ICmd<T> {
    fun run(options: HashMap<String, Any>): Result<T>
}