package io.github.vincenzopalazzo.lnarango.actions.strategies

import io.github.vincenzopalazzo.lnarango.model.ln.ListChannels

/**
 * Strategy to get the list of the channels, we need a strategy because
 * we can talk with the ln node in different way.
 * With REST server, or with the Unix Socket call.
 *
 * @author https://github.com/vincenzopalazzo
 */
interface IRetrievalStrategy<T> {
    fun call(options: HashMap<String, Any> = HashMap()): T
}