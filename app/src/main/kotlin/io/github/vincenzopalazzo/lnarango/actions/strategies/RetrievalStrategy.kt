package io.github.vincenzopalazzo.lnarango.actions.strategies

import io.github.vincenzopalazzo.lnarango.actions.strategies.channelinfo.NodeInfoHttpStrategy
import io.github.vincenzopalazzo.lnarango.actions.strategies.channelinfo.NodeInfoMockStrategy
import io.github.vincenzopalazzo.lnarango.actions.strategies.channelinfo.NodeInfoUnixStrategy
import io.github.vincenzopalazzo.lnarango.actions.strategies.listchannels.ListChannelsHttpStrategy
import io.github.vincenzopalazzo.lnarango.actions.strategies.listchannels.ListChannelsMockStrategy
import io.github.vincenzopalazzo.lnarango.actions.strategies.listchannels.ListChannelsUnixStrategy
import io.github.vincenzopalazzo.lnarango.model.ln.NodeInfo
import io.github.vincenzopalazzo.lnarango.model.ln.ListChannels

enum class Strategy {
    HTTP,
    UNIX,
    MOCK,
}

/**
 * @author https://github.com/vincenzopalazzo
 */
class RetrievalStrategy(strategy: Strategy = Strategy.MOCK) {

    companion object {
        private const val LIST_CHANNEL = "LIST_CHANNEL"
        private const val NODE_INFO = "NODE_INFO"
    }

    var mapping = HashMap<String, IRetrievalStrategy<*>>()

    init {
        val listChannels: IRetrievalStrategy<ListChannels>
        val getNodeInfo: IRetrievalStrategy<NodeInfo>
        when (strategy) {
            Strategy.HTTP -> {
                listChannels = ListChannelsHttpStrategy()
                getNodeInfo = NodeInfoHttpStrategy()
            }
            Strategy.UNIX -> {
                listChannels = ListChannelsUnixStrategy()
                getNodeInfo = NodeInfoUnixStrategy()
            }
            Strategy.MOCK -> {
                listChannels = ListChannelsMockStrategy()
                getNodeInfo = NodeInfoMockStrategy()
            }
        }
        mapping[LIST_CHANNEL] = listChannels
        mapping[NODE_INFO] = getNodeInfo
    }

    fun listChannels(key: String = "listchannels"): ListChannels {
        checkKeyInMap(LIST_CHANNEL)
        return mapping[LIST_CHANNEL]!!.call() as ListChannels
    }

    fun getNodeInfo(nodeId: String): NodeInfo {
        checkKeyInMap(NODE_INFO)
        return mapping[NODE_INFO]!!.call(hashMapOf("node_id" to nodeId)) as NodeInfo
    }

    private fun checkKeyInMap(key: String) {
        if (!mapping.containsKey(key))
            throw IllegalArgumentException("Function mapping with the key %s it is not present".format(key))
    }
}