package io.github.vincenzopalazzo.lnarango.actions.strategies.channelinfo

import io.github.vincenzopalazzo.lnarango.actions.strategies.IRetrievalStrategy
import io.github.vincenzopalazzo.lnarango.actions.strategies.listchannels.ListChannelsHttpStrategy
import io.github.vincenzopalazzo.lnarango.model.ln.ListNodes
import io.github.vincenzopalazzo.lnarango.model.ln.NodeInfo
import io.github.vincenzopalazzo.lnarango.utils.HttpRequestFactory
import io.github.vincenzopalazzo.lnarango.utils.JSONConverter
import mu.KotlinLogging
import okhttp3.MediaType.Companion.toMediaType

class NodeInfoHttpStrategy : IRetrievalStrategy<NodeInfo> {

    companion object {
        private val LOGGER = KotlinLogging.logger { }
    }

    override fun call(options: HashMap<String, Any>): NodeInfo {
        if (!options.containsKey("node_id"))
            throw IllegalArgumentException("node_id key not found")
        val nodeId = options["node_id"]!! as String
        val payload = HashMap<String, String>()
        payload["nodeId"] = nodeId
        val listNodesReq =
            HttpRequestFactory.createRequest(
                url = "network/listnodes",
                type = "form",
                form = payload,
                mediaType = "application/x-www-form-urlencoded".toMediaType()
            )
        val listNodes: ListNodes = HttpRequestFactory.execRequest(
            listNodesReq,
            ListNodes::class.java
        )
        if (listNodes.nodes == null)
            throw Exception("List Nodes null")
        if (listNodes.nodes!!.isEmpty())
            throw Exception("Node with id %s not found".format(nodeId))
        LOGGER.debug { "Request ${JSONConverter.serialize(listNodes)}" }
        return listNodes.nodes!![0]
    }
}