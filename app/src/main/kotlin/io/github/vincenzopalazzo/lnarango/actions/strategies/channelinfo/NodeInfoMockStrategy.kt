package io.github.vincenzopalazzo.lnarango.actions.strategies.channelinfo

import io.github.vincenzopalazzo.lnarango.actions.strategies.IRetrievalStrategy
import io.github.vincenzopalazzo.lnarango.model.ln.NodeInfo
import io.github.vincenzopalazzo.lnarango.utils.JSONConverter
import jrpc.clightning.model.CLightningListNodes

class NodeInfoMockStrategy: IRetrievalStrategy<NodeInfo> {

    var cache: HashMap<String, NodeInfo> = HashMap()

    init {
        val listNodesJson = String(this.javaClass.classLoader.getResourceAsStream("listnodes.json").readAllBytes())
        val listNodes = JSONConverter.deserialize<CLightningListNodes>(listNodesJson, CLightningListNodes::class.java)
        for (node in listNodes.nodes!!) {
            val internalNode = NodeInfo()
            internalNode.bind(node)
            cache[node.nodeId] = internalNode
        }
    }

    override fun call(options: HashMap<String, Any>): NodeInfo {
        if (!options.containsKey("node_id"))
            throw IllegalArgumentException("node_id key not found")
        val nodeId = options["node_id"]!! as String
        if (cache.containsKey(nodeId)) {
            return cache[nodeId]!!
        }
        throw IllegalArgumentException("Node not found")
    }
}