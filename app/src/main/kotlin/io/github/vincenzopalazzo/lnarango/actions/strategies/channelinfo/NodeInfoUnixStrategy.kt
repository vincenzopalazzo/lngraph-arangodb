package io.github.vincenzopalazzo.lnarango.actions.strategies.channelinfo

import io.github.vincenzopalazzo.lnarango.actions.strategies.IRetrievalStrategy
import io.github.vincenzopalazzo.lnarango.model.ln.NodeInfo
import jrpc.clightning.CLightningRPC
import jrpc.clightning.model.types.CLightningNode

class NodeInfoUnixStrategy: IRetrievalStrategy<NodeInfo> {
    override fun call(options: HashMap<String, Any>): NodeInfo {
        if (!options.containsKey("node_id"))
            throw IllegalArgumentException("node_id key not found")
        val key = options["node_id"]!! as String
        val listNode = CLightningRPC.getInstance().listNodes(key)
        if (listNode.nodes.isEmpty())
            throw Exception("Node with id %s not found".format(key))
        val nodeInfo = listNode.nodes[0] as CLightningNode
        val infoNode = NodeInfo()
        infoNode.bind(nodeInfo)
        return infoNode
    }
}