package io.github.vincenzopalazzo.lnarango.actions.strategies.listchannels

import io.github.vincenzopalazzo.lnarango.actions.strategies.IRetrievalStrategy
import io.github.vincenzopalazzo.lnarango.model.ln.ListChannels
import io.github.vincenzopalazzo.lnarango.utils.HttpRequestFactory
import io.github.vincenzopalazzo.lnarango.utils.JSONConverter
import jrpc.wrapper.response.RPCResponseWrapper
import mu.KotlinLogging
import kotlin.reflect.typeOf
import com.google.gson.reflect.TypeToken as TypeToken

class ListChannelsHttpStrategy: IRetrievalStrategy<ListChannels> {

    companion object {
        private val LOGGER = KotlinLogging.logger { }
    }

    override fun call(options: HashMap<String, Any>): ListChannels {
        val listChannelsRequest = HttpRequestFactory.createRequest("networkchannels")
        val type = typeOf<RPCResponseWrapper<ListChannels>>().javaClass
        val channels: RPCResponseWrapper<ListChannels> = HttpRequestFactory.execRequest(
            listChannelsRequest,
            type
        )
        LOGGER.debug { "Request ${JSONConverter.serialize(channels)}" }
        return channels.result
    }
}