package io.github.vincenzopalazzo.lnarango.actions.strategies.listchannels

import io.github.vincenzopalazzo.lnarango.actions.strategies.IRetrievalStrategy
import io.github.vincenzopalazzo.lnarango.model.ln.ListChannels
import io.github.vincenzopalazzo.lnarango.utils.JSONConverter
import jrpc.clightning.model.CLightningListChannels
import mu.KotlinLogging

class ListChannelsMockStrategy: IRetrievalStrategy<ListChannels> {
    companion object {
        private val LOGGER = KotlinLogging.logger { }
    }

    override fun call(options: HashMap<String, Any>): ListChannels {
        val listChannelsJson = String(this.javaClass.classLoader.getResourceAsStream("listchannels.json").readAllBytes())
        val listChannels = JSONConverter.deserialize<CLightningListChannels>(listChannelsJson, CLightningListChannels::class.java)
        LOGGER.info { "Original size is %s".format(listChannels.channels.size) }
        val result = ListChannels()
        result.bind(listChannels)
        return result
    }
}