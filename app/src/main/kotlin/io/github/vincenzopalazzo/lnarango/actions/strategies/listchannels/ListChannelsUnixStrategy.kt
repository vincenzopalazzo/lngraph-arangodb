package io.github.vincenzopalazzo.lnarango.actions.strategies.listchannels

import io.github.vincenzopalazzo.lnarango.actions.strategies.IRetrievalStrategy
import io.github.vincenzopalazzo.lnarango.model.ln.ListChannels
import jrpc.clightning.CLightningRPC

class ListChannelsUnixStrategy: IRetrievalStrategy<ListChannels> {
    override fun call(options: HashMap<String, Any>): ListChannels {
        val listChannel = CLightningRPC.getInstance().listChannels()!!
        val wrapper = ListChannels()
        wrapper.bind(listChannel)
        return wrapper
    }
}