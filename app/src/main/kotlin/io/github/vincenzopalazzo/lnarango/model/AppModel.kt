package io.github.vincenzopalazzo.lnarango.model

object AppModel {
    private val cache = HashMap<String, Any>()

    fun putObject(key: String, value: Any) {
        this.cache[key] = value
    }

    fun getObject(key: String): Any? {
        if (this.cache.containsKey(key))
            return this.cache[key]!!
        return null
    }
}