package io.github.vincenzopalazzo.lnarango.model

import com.arangodb.entity.DocumentField
import com.arangodb.entity.DocumentField.Type

class GraphEdge<T>(
    @DocumentField(Type.FROM) private var from: T,
    @DocumentField(Type.TO) private var to: T,
    val info: HashMap<String, Any>?
) {

    @DocumentField(Type.ID)
    var id: String? = null

    @DocumentField(Type.REV)
    var revision: String? = null
}