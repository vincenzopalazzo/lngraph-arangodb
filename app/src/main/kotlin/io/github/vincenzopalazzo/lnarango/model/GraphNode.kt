package io.github.vincenzopalazzo.lnarango.model

import com.arangodb.entity.DocumentField
import com.arangodb.entity.DocumentField.Type

/**
 * The General concept of node in the graph.
 * In this case the label it is the key, and it is the nodeId.
 * However, we need to cache the node because the key it is unique, look GraphNodeBuilder.
 *
 * As label value we need to store information about this node, such as the addresses and the
 * network information of the node.
 *
 * @author https://github.com/vincenzopalazzo
 */
class GraphNode(
    @DocumentField(Type.KEY) val label: String,
    val value: Map<String, Any>) {

    @DocumentField(Type.ID)
    var id: String? = null

    @DocumentField(Type.REV)
    var revision: String? = null
}