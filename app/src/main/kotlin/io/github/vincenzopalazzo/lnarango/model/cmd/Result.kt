package io.github.vincenzopalazzo.lnarango.model.cmd

data class Result<T>(val value: T)