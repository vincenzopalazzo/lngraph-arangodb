package io.github.vincenzopalazzo.lnarango.model.ln

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import jrpc.clightning.model.CLightningListChannels
import jrpc.clightning.model.types.CLightningChannel
import java.math.BigInteger
import java.util.*
import kotlin.collections.ArrayList

class ListChannels {
    @Expose
    val channels: ArrayList<Channel> = ArrayList()

    fun bind(listChannels: CLightningListChannels) {
        for (channel in listChannels.channels) {
            val wrapper = Channel()
            wrapper.bind(channel)
            channels.add(wrapper)
        }
    }
}

class Channel: CLightningChannel() {

    @Expose
    @SerializedName("shortChannelId")
    var channelId: String? = null
    // avoid a long bind method, in case the bind method
    // it is used, to make small the method, also because
    // we don't need all the properties bind in this class
    var wrapper: CLightningChannel? = null

    fun bind(channel: CLightningChannel) {
        wrapper = channel
        channelId = channel.shortChannelId
    }

    override fun getSource(): String {
        return wrapper?.source ?: super.getSource()
    }

    override fun getDestination(): String {
        return wrapper?.destination ?: super.getDestination()
    }

    override fun getShortChannelId(): String {
        return this.channelId!!
    }

    override fun isPublicChannel(): Boolean {
        return wrapper?.isPublicChannel ?: super.isPublicChannel()
    }

    override fun getSatoshis(): Long {
        return wrapper?.satoshis ?: super.getSatoshis()
    }

    override fun getAmountSat(): String {
        return wrapper?.amountSat ?: super.getAmountSat()
    }

    override fun getMessageFlags(): Int {
        return wrapper?.messageFlags ?: super.getMessageFlags()
    }

    override fun getChannelFlags(): Int {
        return wrapper?.channelFlags ?: super.getChannelFlags()
    }

    override fun isActive(): Boolean {
        return wrapper?.isActive ?: super.isActive()
    }

    override fun getLastUpdate(): Date {
        return wrapper?.lastUpdate ?: super.getLastUpdate()
    }

    override fun getBaseFeeMilliSatoshi(): BigInteger {
        return wrapper?.baseFeeMilliSatoshi ?: (super.getBaseFeeMilliSatoshi() ?: BigInteger.ZERO)
    }

    override fun getFeePerMillionth(): Long {
        return wrapper?.feePerMillionth ?: super.getFeePerMillionth()
    }

    override fun getDelay(): Long {
        return wrapper?.delay ?: super.getDelay()
    }

    override fun getHtlcMinimumMSat(): String {
        return wrapper?.htlcMinimumMSat ?: super.getHtlcMinimumMSat()
    }

    override fun getHtlcMaximumMSat(): String {
        return wrapper?.htlcMaximumMSat ?: super.getHtlcMaximumMSat()
    }
}
