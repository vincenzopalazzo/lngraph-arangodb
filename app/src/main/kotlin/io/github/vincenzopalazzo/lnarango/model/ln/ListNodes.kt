package io.github.vincenzopalazzo.lnarango.model.ln

import com.google.gson.annotations.Expose

class ListNodes {
    @Expose
    var nodes: ArrayList<NodeInfo>? = null
}