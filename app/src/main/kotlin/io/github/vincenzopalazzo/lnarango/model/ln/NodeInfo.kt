package io.github.vincenzopalazzo.lnarango.model.ln

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import jrpc.clightning.model.types.CLightningNode
import jrpc.clightning.model.types.NetworkAddress
import java.math.BigInteger

class NodeInfo: CLightningNode() {
    @Expose
    @SerializedName("nodeId")
    var nodeIdWrapper: String? = null
    var wrapper: CLightningNode? = null

    fun bind(nodeInfo: CLightningNode) {
        wrapper = nodeInfo
    }

    override fun getAddresses(): MutableList<NetworkAddress> {
        return wrapper?.addresses ?: (super.getAddresses() ?: arrayListOf())
    }

    override fun getAlias(): String {
        return wrapper?.alias ?: (super.getAlias() ?: "undefined")
    }

    override fun getNodeId(): String {
        return wrapper?.nodeId ?: nodeIdWrapper!!
    }

    override fun getColor(): String {
        return wrapper?.color ?: (super.getColor() ?: "undefined")
    }

    override fun getLastTimestamp(): BigInteger {
        return wrapper?.lastTimestamp ?: (super.getLastTimestamp() ?: BigInteger.ZERO)
    }
}