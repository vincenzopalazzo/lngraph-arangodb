package io.github.vincenzopalazzo.lnarango.persistence

interface IDAODatabase<T> {
    /**
     * Init the database.
     */
    fun initDatabase(nameDB: String = "lnnetwork", host: String = "localhost", port: String = "8529", pass: String)

    /**
     * Shutdown the database
     */
    fun shutdown()

    /**
     * Remove the database
     */
    fun destroy(nameDB: String = "lnnetwork")

    /**
     * Get the reference of the database
     */
    fun getDatabase(): T
}