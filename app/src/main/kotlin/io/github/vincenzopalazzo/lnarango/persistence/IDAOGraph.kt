package io.github.vincenzopalazzo.lnarango.persistence

import com.arangodb.ArangoDatabase
import io.github.vincenzopalazzo.lnarango.model.GraphNode

interface IDAOGraph<T> {

    /**
     * Create the graph in the database bind with the
     * method build.
     */
    fun createGraph(database: IDAODatabase<ArangoDatabase>, nameGraph: String, directed: Boolean = true)

    /**
     * Insert an edge in the graph.
     */
    fun insertEdge(fromNode: GraphNode, toNode: GraphNode, edgeInfo: HashMap<String, Any>)
}