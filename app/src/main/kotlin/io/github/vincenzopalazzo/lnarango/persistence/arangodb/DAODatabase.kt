package io.github.vincenzopalazzo.lnarango.persistence.arangodb

import com.arangodb.ArangoDB;
import com.arangodb.ArangoDatabase
import com.arangodb.Protocol
import com.arangodb.mapping.ArangoJack
import io.github.vincenzopalazzo.lnarango.persistence.IDAODatabase

object DAODatabase: IDAODatabase<ArangoDatabase> {

    private lateinit var dbBuilder: ArangoDB
    private lateinit var database: ArangoDatabase

    override fun initDatabase(nameDB: String, host: String, port: String, pass: String) {
        dbBuilder = ArangoDB.Builder()
            .host(host, port.toInt())
            .password(pass)
            .serializer(ArangoJack())
            .build()
        if (!dbBuilder.db(nameDB).exists())
            dbBuilder.createDatabase(nameDB)
        database = dbBuilder.db(nameDB)
    }

    override fun shutdown() {
        dbBuilder.shutdown()
    }

    override fun destroy(nameDB: String) {
        TODO("Not yet implemented")
    }

    override fun getDatabase(): ArangoDatabase {
        return this.database
    }
}