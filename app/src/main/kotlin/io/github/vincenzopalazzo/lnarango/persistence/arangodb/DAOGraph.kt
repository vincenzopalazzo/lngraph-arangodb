package io.github.vincenzopalazzo.lnarango.persistence.arangodb

import com.arangodb.ArangoDatabase
import com.arangodb.ArangoGraph
import com.arangodb.entity.EdgeDefinition
import com.arangodb.entity.VertexEntity
import io.github.vincenzopalazzo.lnarango.model.GraphEdge
import io.github.vincenzopalazzo.lnarango.model.GraphNode
import io.github.vincenzopalazzo.lnarango.persistence.IDAODatabase
import io.github.vincenzopalazzo.lnarango.persistence.IDAOGraph
import mu.KotlinLogging

class DAOGraph: IDAOGraph<ArangoDatabase> {

    companion object {
        private val LOGGER = KotlinLogging.logger {  }
    }

    private lateinit var graph: ArangoGraph
    private lateinit var vertexLabel: String
    private lateinit var edgeLabel: String
    private var vertexCache = HashMap<String, VertexEntity>()

    override fun createGraph(database: IDAODatabase<ArangoDatabase>, nameGraph: String, directed: Boolean) {
        if (database.getDatabase().graph(nameGraph).exists())
            database.getDatabase().graph(nameGraph).drop()
        val definitions = ArrayList<EdgeDefinition>()
        this.vertexLabel = "%s_vertex".format(nameGraph)
        this.edgeLabel = "%s_edge".format(nameGraph)
        val edgeDefinition = defineGraphEdge(edgeLabel, vertexLabel)
        definitions.add(edgeDefinition)
        database.getDatabase().createGraph(nameGraph, definitions)
        this.graph = database.getDatabase().graph(nameGraph)
    }

    override fun insertEdge(fromNode: GraphNode, toNode: GraphNode, edgeInfo: HashMap<String, Any>) {
        val fromVertex = createVertex(fromNode)
        val toVertex = createVertex(toNode)
        LOGGER.info { "Create an edge: ${fromNode.label} --> ${toNode.label}" }
        this.createEdge(fromVertex, toVertex, edgeInfo)
    }

    private fun defineGraphEdge(edgeCollName: String, vertexCollName: String): EdgeDefinition {
        return EdgeDefinition().collection(edgeCollName).from(vertexCollName).to(vertexCollName)
    }

    private fun createVertex(fromNode: GraphNode): VertexEntity {
        if (vertexCache.contains(fromNode.label))
            return vertexCache[fromNode.label]!!
        val vertex = this.graph.vertexCollection(this.vertexLabel).insertVertex(fromNode)
        vertexCache[fromNode.label] = vertex
        return vertex
    }

    private fun createEdge(fromVertex: VertexEntity, toVertex: VertexEntity, edgeInfo: HashMap<String, Any> = HashMap()) {
        if (!edgeInfo.containsKey("channel_id"))
            throw Exception("To create an edge we need a channels id info")
        val edge = GraphEdge(from = fromVertex.id, to=toVertex.id, info=edgeInfo)
        this.graph.edgeCollection(this.edgeLabel).insertEdge(edge)
        edgeInfo.remove("channel_id")
    }
}