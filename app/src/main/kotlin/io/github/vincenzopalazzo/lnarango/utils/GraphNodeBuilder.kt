package io.github.vincenzopalazzo.lnarango.utils

import io.github.vincenzopalazzo.lnarango.model.GraphNode

/**
 * TODO: Make a graph builder.
 * TODO Use the bloom filter, and if it tell us that it is present in, we update the node
 * I think that guava have same bloom filter implementation.
 */
object GraphNodeBuilder {

    private val nodeCache = HashMap<String, GraphNode>()

    fun build(label: String, value: Map<String, Any> = HashMap()): GraphNode {
        if (nodeCache.containsKey(label))
            return nodeCache[label]!!
        val graphNode = GraphNode(label, value)
        nodeCache[label] = graphNode
        return graphNode
    }
}