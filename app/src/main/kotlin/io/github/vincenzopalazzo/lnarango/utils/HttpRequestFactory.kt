package io.github.vincenzopalazzo.lnarango.utils

import mu.KotlinLogging
import okhttp3.*
import java.lang.reflect.Type
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.util.concurrent.TimeUnit

/**
 * @author https://github.com/vincenzopalazzo
 */
object HttpRequestFactory {

    private val LOGGER = KotlinLogging.logger {  }
    private lateinit var baseUrl: String
    private var client = OkHttpClient.Builder()
        .retryOnConnectionFailure(true)
        // the timeout it is but, because are the rest API over
        // a JSONRPC 2.0 that doesn't implement a paginator method
        // So the request can be very slow.
        .connectTimeout(60, TimeUnit.MINUTES)
        .writeTimeout(60, TimeUnit.MINUTES)
        .readTimeout(60, TimeUnit.MINUTES)
        .build()

    fun changeBaseUrl(baseUrl: String) {
        this.baseUrl = baseUrl
    }

    fun createRequest(
        url: String,
        type: String = "get",
        body: String = "",
        form: HashMap<String, String>? = null,
        mediaType: MediaType = "application/json; charset=utf-8".toMediaType()
    ): Request {
        val completeUrl = "%s/%s".format(baseUrl, url)
        LOGGER.info { "Request to: $completeUrl" }
        return when (type) {
            "get" -> buildGetRequest(completeUrl)
            "post" -> buildPostRequest(completeUrl, body, mediaType)
            "form" -> buildFormRequest(completeUrl, form!!, mediaType)
            else -> throw IllegalArgumentException("Unsupported method %s".format(type))
        }
    }

    /**
     * This method is designed to retry the request 4 time and wait an exponential time
     * this, the wait time is set to 1 minutes by default and the wait time is exponential,
     * So this mean that the wait time is set to
     */
    @Throws(Exception::class)
    fun <T> execRequest(request: Request, returnType: Type): T {
        try {
            val response = makeRequest(request)
            if (!response.isSuccessful)
                throw Exception("Response fail with code %d".format(response.code))
            val body = response.body?.string()
            return JSONConverter.deserialize(body!!, returnType)
        } catch (ex: Exception) {
            throw ex
        }
    }

    private fun makeRequest(request: Request): Response {
        return client.newCall(request).execute()
    }

    private fun buildFormRequest(url: String, body: HashMap<String, String>, mediaType: MediaType): Request {
        val requestBody = FormBody.Builder()
        body.forEach { (key, value) -> requestBody.add(key, value) }
        return Request.Builder()
            .url(url)
            .post(requestBody.build())
            .build()
    }

    private fun buildPostRequest(url: String, body: String, mediaType: MediaType): Request {
        val requestBody = body.toRequestBody(mediaType)
        return Request.Builder()
            .url(url)
            .post(requestBody)
            .build()
    }

    private fun buildGetRequest(url: String): Request {
        return Request.Builder()
            .url(url)
            .build()
    }
}