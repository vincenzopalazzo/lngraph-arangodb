package io.github.vincenzopalazzo.lnarango.utils

import jrpc.service.converters.JsonConverter
import java.lang.reflect.Type

/**
 * @author https://github.com/vincenzopalazzo
 */
object JSONConverter {

    private val gson = JsonConverter()

    fun serialize(obj: Any): String? {
        return gson.serialization(obj)
    }

    fun <T> deserialize(fromString: String, responseType: Type): T {
        try {
            return (gson.deserialization(fromString, responseType) as T?)!!
        } catch (ex: Exception) {
            ex.printStackTrace()
            throw RuntimeException(ex.cause)
        }
    }
}